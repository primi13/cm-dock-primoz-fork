/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * This version is licensed under GNU-LGPL version 3.0 with support from
 * the University of Barcelona.
 * http://rdock.sourceforge.net/
 ***********************************************************************/

#include "RbtRand.h"
#include "Singleton.h"

/////////////
// Constructor
RbtRand::RbtRand() {
  // Seed the random number generator
  // Fixed seed in debug mode
  // Seed from the random device in release mode
#ifdef _DEBUG
  Seed();
#else  //_DEBUG
  SeedFromRandomDevice();
#endif //_DEBUG
  _RBTOBJECTCOUNTER_CONSTR_("RbtRand");
}

/////////////
// Destructor
RbtRand::~RbtRand() { _RBTOBJECTCOUNTER_DESTR_("RbtRand"); }

////////////////
// Public methods

// Seed the random number generator
void RbtRand::Seed(int seed) { m_rng.seed(seed); }

// Seed the random number generator from the random device
void RbtRand::SeedFromRandomDevice() {
#if defined(__sun)
  std::random_device rd;
  m_rng.seed(rd());
#else
  pcg_extras::seed_seq_from<std::random_device> seedSource;
  m_rng.seed(seedSource);
#endif
}

// Get a flag for cross-platform compatability
bool RbtRand::GetCrossPlatform() {
  return m_cross_platform;
}

// Set a flag for cross-platform compatability
void RbtRand::SetCrossPlatform(bool x) {
  m_cross_platform = x;
}

// Get a random double between 0 and 1
double RbtRand::GetRandom01() {  
  if (m_cross_platform){
    double r = generate_uniform_real(0.0, 1.0);
    Assert<RbtAssert>(r >= 0.0 && r <= 1.0);
    return r;
  }
  std::uniform_real_distribution<> uniformDist(0.0, 1.0);
  return uniformDist(m_rng);
}

// Get a random integer between 0 and nMax-1
int RbtRand::GetRandomInt(int nMax) {
  if (nMax == 0) {
    return nMax;
  }
  // We are using cross-platform implementation
  if (m_cross_platform) {
    int r = generate_uniform_int(0, nMax - 1);
    Assert<RbtAssert>(r >= 0 && r < nMax);
    return r;
  }
  // We are using the standard library
  std::uniform_int_distribution<> uniformDist(0, nMax - 1);
  return uniformDist(m_rng);
}

// Get a random unit vector distributed evenly over the surface of a sphere
RbtVector RbtRand::GetRandomUnitVector() {
  double z = 2.0 * GetRandom01() - 1.0;
  double t = 2.0 * M_PI * GetRandom01();
  double w = std::sqrt(1.0 - z * z);
  double x = w * std::cos(t);
  double y = w * std::sin(t);
  return RbtVector(x, y, z);
}

// Get a random number from the Cauchy distribution (mean, variance)
double RbtRand::GetCauchyRandom(double mean, double variance) {
  if (m_cross_platform) {
    return generate_cauchy(mean, variance);
  }
  std::cauchy_distribution<> cauchyDist{mean, variance};
  return cauchyDist(m_rng);
}

///////////////////////////////////////
// Cross-platform implementation of
// probability distribution functions
// Code base provided by the Boost Random library
// distributed under the Boost Software License, Version 1.0. 
// (See http://www.boost.org/LICENSE_1_0.txt)

double RbtRand::generate_uniform_real(double a = 0.0, double b = 1.0) {
  // Draw a random sample from the continuous uniform distribution
  for (;;) {
    double numerator = static_cast<double>(m_rng() - (m_rng.min)());
    double divisor = static_cast<double>((m_rng.max)() - (m_rng.min)());
    
    if (divisor <= 0) {
      throw RbtError(_WHERE_, "Divisor should be larger than 0.");
    }
    if (!(numerator >= 0 && numerator <= divisor)) {
      throw RbtError(_WHERE_, "Numerator should be >= 0 and <= divisor.");
    }
    double result = numerator / divisor * (b - a) + a;
    // Non-inclusive upper bound, i.e., result must be strictly less than b.
    if (result < b) 
      return result;
  }
}

int RbtRand::generate_uniform_int(int a = 0, int b = 1) {
  // Draw a random sample from the discrete uniform distribution
  typedef unsigned int base_unsigned;

  int range = b - a;
  int bmin = m_rng.min();
  base_unsigned brange = m_rng.max() - bmin;

  if (range == 0) {
    return a;
  }
  else if (brange == range) {
    base_unsigned v = m_rng() - bmin;
    return a + v; // TODO: be careful of overflow!
  }
  else if (brange < range) {
    // Make multiple calls to RNG
    for (;;) {
      int limit;
      if (range == (std::numeric_limits<int>::max)()) {
        limit = range / (brange + 1);
        if (range % (brange + 1) == brange) {
          ++limit;
        }
      }
      else {
        limit = (range + 1) / (brange + 1);
      }

      int result = 0;
      int mult = 1;
      while (mult <= limit) {
        result += (m_rng() - bmin) * mult;
        // equivalent to (mult * (brange+1)) == range+1, but avoids overflow.
        if (mult * brange == range - mult + 1) {
          // The destination range is an integer power of the generator's range.
          return result;
        }
        mult *= brange + 1;
      }

      int result_increment = generate_uniform_int(0, range / mult);
      if ((std::numeric_limits<int>::max)() / mult < result_increment) {
        // The multiplcation would overflow.  Reject immediately.
        continue;
      }
      result_increment *= mult;
      // Unsigned integers are guaranteed to wrap on overflow.
      result += result_increment;
      if (result < result_increment) {
        // The addition overflowed. Reject.
        continue;
      }
      if (result > range) {
        // Too big. Reject.
        continue;
      }
      return result + a;
    }
  }
  else { // brange > range
    base_unsigned bucket_size;
    if (brange == (std::numeric_limits<base_unsigned>::max)()) {
      bucket_size = static_cast<base_unsigned>(brange) / (static_cast<base_unsigned>(range) + 1);
      if (static_cast<base_unsigned>(brange) % (static_cast<base_unsigned>(range) + 1) == static_cast<base_unsigned>(range)) {
        ++bucket_size;
      }
    }
    else {
      bucket_size = static_cast<base_unsigned>(brange + 1) / (static_cast<base_unsigned>(range) + 1);
    }
    for (;;) {
      base_unsigned result = m_rng() - bmin;
      result /= bucket_size;
      // result and range are non-negative, and result is possibly larger
      // than range, so the cast is safe
      if (result <= static_cast<base_unsigned>(range)) {
        return result + a;
      }
    }
  }
}

double RbtRand::generate_cauchy(double median = 0.0, double sigma = 1.0) {
  // Draw a random sample from the Cauchy distribution
  const double pi = 3.14159265358979323846;
  double val = generate_uniform_real(0.0, 1.0) - 0.5;
  return median + sigma * std::tan(pi * val);
}


///////////////////////////////////////
// Non-member functions in Rbt namespace

// Returns reference to single instance of RbtRand class (singleton)
RbtRand &Rbt::GetRbtRand() { return Singleton<RbtRand>::instance(); }
