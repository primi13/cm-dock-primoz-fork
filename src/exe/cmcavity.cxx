/***********************************************************************
 * The rDock program was developed from 1998 - 2006 by the software team
 * at RiboTargets (subsequently Vernalis (R&D) Ltd).
 * In 2006, the software was licensed to the University of York for
 * maintenance and distribution.
 * In 2012, Vernalis and the University of York agreed to release the
 * program as Open Source software.
 * Version with support from the University of Barcelona is found at:
 * http://rdock.sourceforge.net/
 * From 2019 RxTx developed a fork of rDock 
 * called RxDock (modern rewrite) at: https://gitlab.com/rxdock
 * From 2020 a fork of RxDock is developed under the name CmDock
 * or CurieMarieDock at: https://gitlab.com/Jukic/cmdock
 * **************licensed under GNU-LGPL version 3.0********************
 ***********************************************************************/

// Standalone executable for generating docking site .as files for cmdock

#include <algorithm>
#include <cxxopts.hpp>
#include <iomanip>
#include <composestream.hpp>

#include "RbtBiMolWorkSpace.h"
#include "RbtCrdFileSink.h"
#include "RbtDockingSite.h"
#include "RbtPRMFactory.h"
#include "RbtParameterFileSource.h"
#include "RbtPsfFileSink.h"
#include "RbtSiteMapperFactory.h"

void PrintHeaderAddon(std::ostream &outputStream) {
  outputStream << "     ***  CmCavity (receptor grid definition)   ***     " << std::endl;
  outputStream << "********************************************************" << std::endl;
  outputStream << std::endl << "Calculates docking cavities." << std::endl;
  outputStream << std::endl;
  outputStream << "Use -h for help." << std::endl;
}

/////////////////////////////////////////////////////////////////////
// MAIN PROGRAM STARTS HERE
/////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
  // Handle obsolete arguments, if any
  for (int i = 0; i < argc; i++) {
    std::string opt = argv[i];
    if (opt == "-was" || opt == "-ras") {
      std::cout
          << "Options -was and -ras are no longer supported; use -W and -R "
             "(respectively) instead."
          << std::endl;
      return 2;
    }
  }

  // Parse the command for later use
  std::string sCommand;
  for (int i = 0; i < argc; ++i) {
      sCommand = sCommand + argv[i] + " ";
  }

  // Strip off the path to the executable, leaving just the file name
  std::string strExeName(argv[0]);
  std::string::size_type i = strExeName.rfind("/");
  if (i != std::string::npos)
    strExeName.erase(0, i + 1);

  // Print a standard header
  Rbt::PrintStdHeader(std::cout, strExeName);

  PrintHeaderAddon(std::cout);

  cxxopts::Options options(strExeName, "Calculates docking cavities.");

  // Command line arguments and default values
  cxxopts::OptionAdder adder = options.add_options();
  adder("r,receptor-param", "receptor param file (contains active site params)",
        cxxopts::value<std::string>());
  adder("o,output", "output file", cxxopts::value<std::string>());
  adder("W,write-docking-cavities",
        "write docking cavities (plus distance grid) to .as file");
  adder("R,read-docking-cavities",
        "read docking cavities (plus distance grid) from .as file");
  adder("I,write-insightii-grids",
        "dump InsightII grids for each cavity for visualisation");
  adder("v,write-psf-crd", "dump target PSF/CRD files for rDock Viewer");
  adder("l,list-atoms-dist",
        "list receptor atoms within specified distance of any cavity (in "
        "angstrom)",
        cxxopts::value<double>()->default_value("5.0"));
  adder("s,print-site", "print SITE descriptors (counts of exposed atoms)");
  adder(
      "b,border",
      "set the border around the cavities for the distance grid (in angstrom)",
      cxxopts::value<double>()->default_value("8.0"));
  //adder("m,write-moe-grid", "write active site into a MOE grid");
  adder("M,write-mrc-grid", "write active site into a MRC/CCP4 2014 grid");
  adder("h,help", "Print help");

  try {
    auto result = options.parse(argc, argv);

    if (result.count("h")) {
      std::cout << options.help() << std::endl;
      return 0;
    }

    std::string strReceptorPrmFile;
    if (result.count("r")) {
      strReceptorPrmFile = result["r"].as<std::string>();
    }
    std::string strOutputOverride;
    bool bCustomOutput = result.count("o");
    if (bCustomOutput) {
      strOutputOverride = result["o"].as<std::string>();
    }
    bool bReadAS = result.count("R");  // If true, read Active Site from file
    bool bWriteAS = result.count("W"); // If true, write Active Site to file
    bool bInsightGrid =
        result.count("I"); // If true, dump cavity grids in Insight format
    bool bViewer =
        result.count("v"); // If true, dump PSF/CRD files for rDock Viewer
    bool bList =
        result.count("l"); // If true, list atoms within 'distance' of cavity
    bool bSite = result.count(
        "s"); // If true, print out "SITE" descriptors (counts of exposed atoms)
    // bool bMOEgrid = result.count(
    //     "m"); // If true, create a MOE grid file for AS visualisation
    bool bMRCgrid = result.count(
        "M"); // If true, create a MRC grid file for AS visualisation
    bool bBorderArg = result.count("b"); // If true, border was specified in the
                                         // command line
    double border =
        result["b"]
            .as<double>(); // Border to allow around cavities for distance grid
    double dist = result["l"].as<double>();

    // check for parameter file name
    /*if(prmFile) {
            strReceptorPrmFile	= prmFile;
    }*/
    if (strReceptorPrmFile.empty()) {
      std::cout << "Missing receptor parameter file name" << std::endl;
      return 1;
    }

    // Open up log file
    ComposeStream log;
    std::ofstream logFile;
    logFile.open(strReceptorPrmFile + ".log");
    log.linkStream(std::cout);
    log.linkStream(logFile);
    logFile << "Command:" << std::endl;
    logFile << sCommand;
    logFile << std::endl;
    Rbt::PrintStdHeader(logFile, strExeName);
    PrintHeaderAddon(logFile);
    logFile << std::endl;

    // writing command line arguments
    log << "Command line arguments:" << std::endl;
    log << "-r " << strReceptorPrmFile << std::endl;
    if (bCustomOutput)
      log << "-o " << strOutputOverride << std::endl;
    if (bList)
      log << "-l " << dist << std::endl;
    if (bBorderArg)
      log << "-b " << border << std::endl;
    if (bWriteAS)
      log << "-W" << std::endl;
    if (bReadAS)
      log << "-R" << std::endl;
    //if (bMOEgrid)
    //  log << "-m" << std::endl;
    if (bMRCgrid)
      log << "-M" << std::endl;
    if (bInsightGrid)
      log << "-I" << std::endl;
    if (bSite)
      log << "-s" << std::endl;
    if (bViewer)
      log << "-v" << std::endl;

    std::cout.setf(std::ios_base::left, std::ios_base::adjustfield);

    // Create a bimolecular workspace
    RbtBiMolWorkSpacePtr spWS(new RbtBiMolWorkSpace());
    // Set the workspace name to the root of the receptor .prm filename
    std::vector<std::string> componentList =
        Rbt::ConvertDelimitedStringToList(strReceptorPrmFile, ".");
    std::string wsName = componentList.front();
    spWS->SetName(wsName);

    // Read the protocol parameter file
    RbtParameterFileSourcePtr spRecepPrmSource(new RbtParameterFileSource(
        Rbt::GetRbtFileName("data/receptors", strReceptorPrmFile)));

    // Create the receptor model from the file names in the parameter file
    spRecepPrmSource->SetSection();
    RbtPRMFactory prmFactory(spRecepPrmSource);
    RbtModelPtr spReceptor = prmFactory.CreateReceptor();

    RbtDockingSitePtr spDockSite;
    std::string strASFile;
	if (bCustomOutput && !bReadAS)
	  strASFile = strOutputOverride;
	else
	  strASFile = wsName + ".as";

    // Either read the docking site from the .as file
    if (bReadAS) {
      std::string strInputFile = Rbt::GetRbtFileName("data/grids", strASFile);
#if defined(__sgi) && !defined(__GNUC__)
      std::ifstream istr(strInputFile.c_str(), std::ios_base::in);
#else
      std::ifstream istr(strInputFile.c_str(),
                         std::ios_base::in | std::ios_base::binary);
#endif
      spDockSite = RbtDockingSitePtr(new RbtDockingSite(istr));
      istr.close();
    }
    // Or map the site using the prescribed mapping algorithm
    else {
      RbtSiteMapperFactoryPtr spMapperFactory(new RbtSiteMapperFactory());
      RbtSiteMapperPtr spMapper =
          spMapperFactory->CreateFromFile(spRecepPrmSource, "MAPPER");
      spMapper->Register(spWS);
      spWS->SetReceptor(spReceptor);
      log << *spMapper << std::endl;

      int nRI = spReceptor->GetNumSavedCoords() - 1;
      if (nRI == 0) {
        spDockSite =
            RbtDockingSitePtr(new RbtDockingSite((*spMapper)(), border));
      } else {
        RbtCavityList allCavs;
        for (int i = 1; i <= nRI; i++) {
          spReceptor->RevertCoords(i);
          RbtCavityList cavs((*spMapper)());
          std::copy(cavs.begin(), cavs.end(), std::back_inserter(allCavs));
        }
        spDockSite = RbtDockingSitePtr(new RbtDockingSite(allCavs, border));
      }
    }

    log << std::endl
        << "DOCKING SITE" << std::endl
        << (*spDockSite) << std::endl;

    if (bWriteAS) {
#if defined(__sgi) && !defined(__GNUC__)
      std::ofstream ostr(strASFile.c_str(),
                         std::ios_base::out | std::ios_base::trunc);
#else
      std::ofstream ostr(strASFile.c_str(), std::ios_base::out |
                                                std::ios_base::binary |
                                                std::ios_base::trunc);
#endif
      spDockSite->Write(ostr, log);
      ostr.close();
    }

    // Write PSF/CRD files to keep the rDock Viewer happy (it doesn't read MOL2
    // files yet)
    if (bViewer) {
      RbtMolecularFileSinkPtr spRecepSink =
          new RbtPsfFileSink(wsName + "_for_viewer.psf", spReceptor);
      log << "Writing PSF file: " << spRecepSink->GetFileName()
          << std::endl;
      spRecepSink->Render();
      spRecepSink = new RbtCrdFileSink(wsName + "_for_viewer.crd", spReceptor);
      log << "Writing CRD file: " << spRecepSink->GetFileName()
          << std::endl;
      spRecepSink->Render();
    }

    // Write an ASCII InsightII grid file for each defined cavity
    if (bInsightGrid || bMRCgrid) {
      RbtCavityList cavList = spDockSite->GetCavityList();
      for (unsigned int i = 0; i < cavList.size(); i++) {
        std::ostringstream filename;
        filename << wsName << "_cav" << i + 1;
        if (bInsightGrid) {
          std::string sFilename = filename.str() + ".grd";
          log << "Writing InsightII Grid file: " << sFilename << std::endl;
          std::ofstream dumpFile(sFilename);
          if (dumpFile) {
            cavList[i]->GetGrid()->PrintInsightGrid(dumpFile);
            dumpFile.close();
          }
        }
        if (bMRCgrid) {
          std::string sFilename = filename.str() + ".mrc";
          log << "Writing MRC/CCP4 Grid file: " << sFilename << std::endl;
          std::ofstream dumpFile(sFilename);
          if (dumpFile) {
            cavList[i]->GetGrid()->PrintMRCGrid(dumpFile);
            dumpFile.close();
          }
        }
      }
    }

    // writing active site into MOE grid
    // if (bMOEgrid) {
    //   log << "MOE grid feature not yet implemented, sorry." << std::endl;
    // }

    // List all receptor atoms within given distance of any cavity
    if (bList) {
      RbtRealGridPtr spGrid = spDockSite->GetGrid();
      RbtAtomList atomList =
          spDockSite->GetAtomList(spReceptor->GetAtomList(), 0.0, dist);
      log << atomList.size() << " receptor atoms within " << dist
          << " A of any cavity" << std::endl;
      log << std::endl << "DISTANCE,ATOM" << std::endl;
      for (RbtAtomListConstIter iter = atomList.begin(); iter != atomList.end();
           iter++) {
        log << spGrid->GetSmoothedValue((*iter)->GetCoords()) << "\t"
            << **iter << std::endl;
      }
      log << std::endl;
    }

    // DM 15 Jul 2002 - print out SITE descriptors
    // Use a crude measure of solvent accessibility - count #atoms within 4A of
    // each atom Use an empirical threshold to determine if atom is exposed or
    // not
    if (bSite) {
      double cavDist = 4.0; // Use a fixed definition of cavity atoms - all
                            // those within 4A of docking volume
      double neighbR = 4.0; // Sphere radius for counting nearest neighbours
      double threshold =
          15; // Definition of solvent exposed: neighbours < threshold
      // RbtRealGridPtr spGrid = spDockSite->GetGrid();
      RbtAtomList recepAtomList = spReceptor->GetAtomList();
      RbtAtomList cavAtomList = spDockSite->GetAtomList(recepAtomList, cavDist);
      RbtAtomList exposedAtomList; // The list of exposed cavity atoms
      log << std::endl << "SOLVENT EXPOSED CAVITY ATOMS" << std::endl;
      log << "1) Consider atoms within " << cavDist << "A of docking site"
          << std::endl;
      log << "2) Determine #neighbours within " << neighbR
          << "A of each atom" << std::endl;
      log << "3) If #neighbours < " << threshold << " => exposed"
          << std::endl;
      log << "4) Calculate SITE* descriptors over exposed cavity atoms only"
          << std::endl;
      log << std::endl << "ATOM NAME\t#NEIGHBOURS" << std::endl;

      // Get the list of solvent exposed cavity atoms
      for (RbtAtomListConstIter iter = cavAtomList.begin();
           iter != cavAtomList.end(); iter++) {
        int nNeighb = Rbt::GetNumAtoms(
            recepAtomList,
            Rbt::isAtomInsideSphere((*iter)->GetCoords(), neighbR));
        nNeighb--;
        if (nNeighb < threshold) {
          log << (*iter)->GetFullAtomName() << "\t" << nNeighb
              << std::endl;
          exposedAtomList.push_back(*iter);
        }
      }

      // Total +ve and -ve charges
      double posChg(0.0);
      double negChg(0.0);
      for (RbtAtomListConstIter iter = exposedAtomList.begin();
           iter != exposedAtomList.end(); iter++) {
        double chg = (*iter)->GetGroupCharge();
        if (chg > 0.0) {
          posChg += chg;
        } else if (chg < 0.0) {
          negChg += chg;
        }
      }

      // Atom type counts
      Rbt::isHybridState_eq bIsArom(RbtAtom::AROM);
      int nAtoms = exposedAtomList.size();
      int nLipoC = Rbt::GetNumAtoms(exposedAtomList, Rbt::isAtomLipophilic());
      int nArom = Rbt::GetNumAtoms(exposedAtomList, bIsArom);
      int nNHBD = Rbt::GetNumAtoms(exposedAtomList, Rbt::isAtomHBondDonor());
      int nMetal = Rbt::GetNumAtoms(exposedAtomList, Rbt::isAtomMetal());
      int nGuan =
          Rbt::GetNumAtoms(exposedAtomList, Rbt::isAtomGuanidiniumCarbon());
      int nNHBA = Rbt::GetNumAtoms(exposedAtomList, Rbt::isAtomHBondAcceptor());

      // Cavity volume
      log << std::endl
          << wsName << ",SITE_VOL," << spDockSite->GetVolume()
          << std::endl;
      // Atom type counts
      log << wsName << ",SITE_NATOMS," << nAtoms << std::endl;
      log << wsName << ",SITE_NLIPOC," << nLipoC << std::endl;
      log << wsName << ",SITE_NAROMATOMS," << nArom << std::endl;
      log << wsName << ",SITE_NHBD," << nNHBD << std::endl;
      log << wsName << ",SITE_NMETAL," << nMetal << std::endl;
      log << wsName << ",SITE_NGUAN," << nGuan << std::endl;
      log << wsName << ",SITE_NHBA," << nNHBA << std::endl;
      // Atom type percentages
      log << wsName << ",SITE_PERC_LIPOC," << 100.0 * nLipoC / nAtoms
          << std::endl;
      log << wsName << ",SITE_PERC_AROMATOMS," << 100.0 * nArom / nAtoms
          << std::endl;
      log << wsName << ",SITE_PERC_HBD," << 100.0 * nNHBD / nAtoms
          << std::endl;
      log << wsName << ",SITE_PERC_METAL," << 100.0 * nMetal / nAtoms
          << std::endl;
      log << wsName << ",SITE_PERC_GUAN," << 100.0 * nGuan / nAtoms
          << std::endl;
      log << wsName << ",SITE_PERC_HBA," << 100.0 * nNHBA / nAtoms
          << std::endl;
      // Charges
      log << wsName << ",SITE_POS_CHG," << posChg << std::endl;
      log << wsName << ",SITE_NEG_CHG," << negChg << std::endl;
      log << wsName << ",SITE_TOT_CHG," << posChg + negChg << std::endl;

      logFile.close();
    }
  } catch (const cxxopts::OptionException &e) {
    std::cout << "Error parsing options: " << e.what() << std::endl;
    return 1;
  } catch (RbtError &e) {
    std::cout << e << std::endl;
  } catch (...) {
    std::cout << "Unknown exception" << std::endl;
  }

  _RBTOBJECTCOUNTER_DUMP_(std::cout)

  return 0;
}