#!/usr/bin/python3
import configparser
import os
import os.path as op
import subprocess
import sys
import shutil
from ftplib import FTP
from pathlib import Path
import time

import logging
import argparse


import gzip
import numpy as np
import multiprocessing

try:
    from pymol import cmd
    no_pymol = False
except ModuleNotFoundError:
    print("No PyMOL, receptor/reference ligand file preperation will not function")
    no_pymol = True

global DOCKING_DIRECTORY,PDBS_DIRECTORY
RUNNING_DIRECTORY = os.getcwd()
PDBS_DIRECTORY = "./PDBs/"  
DOCKING_DIRECTORY = "./Docking/"

logger = logging.getLogger()

class Cavity_Parameters:

    def __init__(self,radius,small_sphere,max_cavities,vol_increase,grid_step,min_volume,two_sphere_center,reference_ligand,large_sphere):
        self.radius = radius
        self.small_sphere = small_sphere
        self.max_cavities = max_cavities
        self.vol_increase = vol_increase
        self.grid_step = grid_step
        self.min_volume = min_volume
        self.two_sphere_center = two_sphere_center
        self.reference_ligand = reference_ligand
        self.large_sphere = large_sphere
    def print(self):
        global logger
        print_str = f'''
#### Cavity Parameters #####
    radius {self.radius}
    small_sphere {self.small_sphere}
    max_cavities {self.max_cavities}
    vol_increase {self.vol_increase}
    grid_step {self.grid_step}
    min_volume {self.min_volume}
    two_sphere_center {self.two_sphere_center}
    reference_ligand {self.reference_ligand}
    large_sphere {self.large_sphere}
'''
        return print_str

class Docking_Parameters:
    def __init__(self,n_docking_runs,n_best_poses,CmCavity_exe,CmDock_exe,ligand_dat,dock_score_parameter,main_results_file):
        self.n_docking_runs = n_docking_runs 
        self.n_best_poses = n_best_poses 
        self.CmCavity_exe = CmCavity_exe 
        self.CmDock_exe = CmDock_exe 
        self.ligand_dat = ligand_dat
        self.dock_score_parameter = dock_score_parameter
        self.main_results_file = main_results_file

    def print(self):
        global logger
        print_str = f'''
#### Docking Parameters #####
    n_docking_runs {self.n_docking_runs}
    n_best_poses {self.n_best_poses}
    CmCavity_exe {self.CmCavity_exe}
    CmDock_exe {self.CmDock_exe}
    ligand_dat {self.ligand_dat}
    dock_score_parameter {self.dock_score_parameter}
    main_results_file {self.main_results_file}
'''
        return print_str




class Receptor:

    def __init__(self,receptor_name,PDB_ID,chains_kept,ligands_kept,cav_param,dock_param,PDB_file,receptor_file,prm_file,subdir):
        self.receptor_name = receptor_name
        self.PDB_ID = PDB_ID
        self.chains_kept = chains_kept
        self.ligands_kept = ligands_kept
        self.cav_param = cav_param
        self.dock_param = dock_param

        self.PDB_file = PDB_file
        self.receptor_file = receptor_file
        self.prm_file = prm_file
        self.subdir = subdir
    def print(self):
        global logger
        print_str = f'''
####### Receptor #######
    Receptor {self.receptor_name}
    PDB_ID {self.PDB_ID}
    chains_kept {self.chains_kept}
    ligands_kept {self.ligands_kept}
    PDB_file {self.PDB_file} {op.isfile(self.PDB_file)}
    receptor_file {self.receptor_file} {op.isfile(self.receptor_file)}
    prm_file {self.prm_file} {op.isfile(self.prm_file)}
    subdir {self.subdir} {op.isdir(self.subdir)}
'''

        print_str += self.cav_param.print()
        print_str += self.dock_param.print()

        logger.info(print_str)




    def Download_PDB_file(self):
        global PDBS_DIRECTORY,logger
        unzip_fn = self.PDB_ID.lower() + ".pdb"

        if op.isfile(op.join(PDBS_DIRECTORY,unzip_fn)):
            logger.info(f"PDB file present, {op.join(PDBS_DIRECTORY,unzip_fn)}")
            self.PDB_file = op.join(PDBS_DIRECTORY,unzip_fn)
            return
        try:
            logger.info(f"Downloading PDB file {self.PDB_ID}")
            ftp_wwpdb_server = FTP("ftp.wwpdb.org")
            ftp_wwpdb_server.login()
            ftp_wwpdb_server.cwd(
                '/pub/pdb/data/structures/divided/pdb/' + str(self.PDB_ID[1:3]).lower() + "/")

            ziped_fn = "pdb" + self.PDB_ID.lower() + ".ent.gz"
            local_file = open(ziped_fn, 'wb')
            ftp_wwpdb_server.retrbinary('RETR ' + ziped_fn, local_file.write)
            local_file.close()
            with gzip.open(ziped_fn, 'rb') as compressed_f:
                local_file_unzip = open(unzip_fn, 'wb')
                local_file_unzip.write(compressed_f.read())
                local_file_unzip.close()

            shutil.move(unzip_fn, os.path.join(PDBS_DIRECTORY,unzip_fn))
            self.PDB_file = op.join(PDBS_DIRECTORY,unzip_fn)
            os.remove(ziped_fn)
        except:
            logger.error(sys.exc_info())
            logger.error(f"Downloading PDB file {self.PDB_ID} failed")
            self.PDB_file = ""




    def Prepare_Receptor_File(self):
        global DOCKING_DIRECTORY,logger

        if op.isfile(self.PDB_file):
            try:
                pymol_output = cmd.do(f"rein")
                #logger.info(pymol_output)
                pymol_output = cmd.do(f"load {self.PDB_file}, receptor_{self.receptor_name}")
                #logger.info(pymol_output)

                if self.chains_kept:
                
                    for i, chain in enumerate(self.chains_kept):

                        pymol_output = cmd.do(f"select to_keep, polymer and chain {chain},merge=1")
                        #logger.info(pymol_output)

                else:
                    pymol_output = cmd.do("select to_keep, polymer") # if no chains_kept defined all chains_kept
                    #logger.info(pymol_output)
                

                if self.ligands_kept:
                    for i, lig in enumerate(self.ligands_kept):
                        lig_name, lig_resi, lig_chain = lig.split("_")
                        pymol_output = cmd.do(f"select to_keep, resi {lig_resi} and chain {lig_chain},merge=1")
                        #logger.info(pymol_output)
                # if no ligands_kept defined, no ligands_kept
                
                rec_fn_start = f"rec_{self.receptor_name}_"
                if not self.chains_kept:
                    rec_fn_start = f"rec_{self.receptor_name}"
                
                for ch in self.chains_kept:
                    rec_fn_start += ch
                
                for lig in self.ligands_kept:
                    rec_fn_start += "_" +  lig

                rec_fn = rec_fn_start +".mol2"

                self.receptor_file = op.join(self.subdir,rec_fn)

                rm_cmd = f"remove not to_keep"
                pymol_output = cmd.do(rm_cmd)
                #logger.info(pymol_output)
                pymol_output = cmd.deselect()     
                #logger.info(pymol_output)
                pymol_output = cmd.save(self.receptor_file, f"receptor_{self.receptor_name}")
                #logger.info(pymol_output)
            except:
                print(sys.exc_info())
                logger.error("Error preparing receptor file")
        else:
            self.receptor_file = ""
            logger.error(f"PDB file {self.PDB_file} not found, receptor file not prepared")


    def Make_Prm_File(self):
        global DOCKING_DIRECTORY,logger
        ref_lig_template = '''RBT_PARAMETER_FILE_V1.00
TITLE REPLACE_prm_title

RECEPTOR_FILE REPLACE_rec_filename

#------------------------------
#cavitiy from ref ligand
SECTION MAPPER
   SITE_MAPPER RbtLigandSiteMapper
   REF_MOL REPLACE_ref_mol_filename
   RADIUS REPLACE_radius
   SMALL_SPHERE REPLACE_small_sphere
   MIN_VOLUME REPLACE_min_vol
   MAX_CAVITIES REPLACE_max_cav
   VOL_INCR REPLACE_vol_incr
   GRIDSTEP REPLACE_gridstep
END_SECTION
#end cav def###########

#------------------------------
#restraint
SECTION CAVITY
   SCORING_FUNCTION RbtCavityGridSF
   WEIGHT 1.0
END_SECTION
#end restraint

#------------------------------

#end prm file definition'''
        two_sphere_template = '''RBT_PARAMETER_FILE_V1.00
TITLE REPLACE_prm_title

RECEPTOR_FILE REPLACE_rec_filename

#------------------------------
#cavitiy two sphere method
SECTION MAPPER
   SITE_MAPPER RbtSphereSiteMapper
   RADIUS REPLACE_radius
   SMALL_SPHERE REPLACE_small_sphere
   LARGE_SPHERE REPLACE_large_sphere
   CENTER REPLACE_center
   MIN_VOLUME REPLACE_min_vol
   MAX_CAVITIES REPLACE_max_cav
   VOL_INCR REPLACE_vol_incr
   GRIDSTEP REPLACE_gridstep
END_SECTION
#end cav def###########

#------------------------------
#restraint
SECTION CAVITY
   SCORING_FUNCTION RbtCavityGridSF
   WEIGHT 1.0
END_SECTION
#end restraint

#------------------------------

#end prm file definition
'''

        if not op.isfile(self.receptor_file):
            self.prm_file = ""
            return
        
        rep_title = "REPLACE_prm_title"
        rep_rec_fn = "REPLACE_rec_filename"
        rep_ref_mol_fn = "REPLACE_ref_mol_filename"
        rep_rad = "REPLACE_radius"
        rep_small_sp = "REPLACE_small_sphere"

        rep_min_vol = "REPLACE_min_vol"
        rep_max_cav = "REPLACE_max_cav"
        rep_vol_incr = "REPLACE_vol_incr"
        rep_gridstep = "REPLACE_gridstep"

        rep_center = "REPLACE_center"
        rep_large_sp = "REPLACE_large_sphere"
        template_lines = []
        if self.cav_param.two_sphere_center: # two sphere
            self.prm_file  = op.join(self.subdir, f"{self.receptor_name}_two_sphere.prm")
            #print(f"#############################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################2sp {self.prm_file} {DOCKING_DIRECTORY}")
            template_lines = two_sphere_template.split("\n")
            title = f"{self.receptor_name}_two_sphere"
            cnt = self.cav_param.two_sphere_center
            cnt = cnt.replace("(","")
            cnt = cnt.replace(")","")
            cnt = cnt.replace("[","")
            cnt = cnt.replace("]","")
            
            center_inp_spl1 = cnt.strip().split()
            center_inp_spl2 = []
            for sp in center_inp_spl1:
                for s in sp.split(","):
                    center_inp_spl2.append(s)

            while "" in center_inp_spl2:
                center_inp_spl2.remove("")
            # print(center_inp_spl2)

            if len(center_inp_spl2) != 3:
                logger.error("error with center 1")
                return 1
            try:
                two_sphere_center = [float(x) for x in center_inp_spl2]
            except:
                logger.error("error with center 2")

                return 1 
            
        
        if self.cav_param.reference_ligand:  # ref ligand
            template_lines = ref_lig_template.split("\n")
            self.prm_file  = op.join(self.subdir,  f"{self.receptor_name}_ref_lig.prm")
            #print(f"##################################################################################################################################################################################################################################################################################################################ref {self.prm_file} {DOCKING_DIRECTORY}")
            title = f"{self.receptor_name}_ref_lig"
            

            
            if not op.isfile(self.cav_param.reference_ligand):

                try:
                    lig_name, lig_resi, lig_chain = self.cav_param.reference_ligand.split("_")
                    pymol_output = cmd.do(f"rein")
                    #logger.info(pymol_output)
                    pymol_output = cmd.do(f"load {self.PDB_file}, receptor_{self.receptor_name}")
                    #logger.info(pymol_output)
                    pymol_output = cmd.do(f"select lig, resi {lig_resi} and chain {lig_chain}")
                    #logger.info(pymol_output)
                    pymol_output = cmd.do(f"extract {lig_name},  lig")
                    #logger.info(pymol_output)
                    lig_save_fn = op.join(self.subdir,f"ligand_{self.cav_param.reference_ligand}_{self.receptor_name}.sdf")
                    pymol_output = cmd.save( lig_save_fn, lig_name)
                    #logger.info(pymol_output)
                            
                    self.cav_param.reference_ligand = lig_save_fn
                    if not op.isfile(self.cav_param.reference_ligand):
                        logger.error("did not find extracted reference ligand file")
                        return 1
                except:
                    logger.error(sys.exc_info)
                    logger.error("did not find reference ligand file, error with extracting ligand")
                    return 1

            if not self.cav_param.reference_ligand.split(".")[-1] == "sdf": # if not a sdf file
                try:
                    pymol_output = cmd.do(f"rein")
                    #logger.info(pymol_output)
                    pymol_output = cmd.do(f"load {self.cav_param.reference_ligand}, lig")
                    #logger.info(pymol_output)
                    lig_save_fn = op.join(self.subdir,f"{Path(self.cav_param.reference_ligand).stem}.sdf")
                    pymol_output = cmd.save( lig_save_fn, "lig")
                    #logger.info(pymol_output)
                    self.cav_param.reference_ligand = lig_save_fn
                    if not op.isfile(self.cav_param.reference_ligand):
                        logger.error("did not find .sdf converted reference ligand file")
                        return 1
                except:
                    logger.error(sys.exc_info)
                    logger.error("error converting reference ligand to .sdf file")

        out_lines = []
        if not template_lines:
            logger.error("Cannot make cavity, referecnce ligand/ two sphere center not defined")
            return 1
        for ln in template_lines:
            if "REPLACE_" in ln:
                if rep_title in ln:
                    out_lines.append(ln.replace(rep_title, title))
                elif rep_rec_fn in ln:
                    rep_rec_file = self.receptor_file # absolute path
                    if op.dirname(self.receptor_file) == self.subdir: # relative path if in same dir
                        rep_rec_file = op.basename(self.receptor_file)
                    out_lines.append(ln.replace(rep_rec_fn, rep_rec_file))
                elif rep_ref_mol_fn in ln:
                    rep_ref_mol_file = self.cav_param.reference_ligand # absolute path
                    if op.dirname(self.cav_param.reference_ligand) == self.subdir: # relative path if in same dir
                        rep_ref_mol_file = op.basename(self.cav_param.reference_ligand)
                    out_lines.append(ln.replace(rep_ref_mol_fn, rep_ref_mol_file))

                elif rep_rad in ln:
                    out_lines.append(ln.replace(
                        rep_rad, str(self.cav_param.radius)))
                elif rep_small_sp in ln:
                    out_lines.append(ln.replace(rep_small_sp, str(
                        self.cav_param.small_sphere)))
                elif rep_min_vol in ln:
                    out_lines.append(ln.replace(
                        rep_min_vol, str(self.cav_param.min_volume)))
                elif rep_max_cav in ln:
                    out_lines.append(ln.replace(
                        rep_max_cav, str(self.cav_param.max_cavities)))
                elif rep_vol_incr in ln:
                    out_lines.append(ln.replace(
                        rep_vol_incr, str(self.cav_param.vol_increase)))
                elif rep_gridstep in ln:
                    out_lines.append(ln.replace(
                        rep_gridstep, str(self.cav_param.grid_step)))
                elif rep_center in ln:
                    center_str = f"({two_sphere_center[0]},{two_sphere_center[1]},{two_sphere_center[2]})"
                    out_lines.append(ln.replace(rep_center, center_str))
                elif rep_large_sp in ln:
                    out_lines.append(ln.replace(rep_large_sp, str(
                        self.cav_param.large_sphere)))

            else:
                out_lines.append(ln)

        with open(self.prm_file, "w") as prm:
            for ln in out_lines:
                prm.write(ln+"\n")

    def Run_CmCavity(self):
        global logger
        if not op.isfile(self.prm_file):
            logger.warning(f"Error, CmCavity no prm file {self.prm_file}")
            return
        as_file = op.join(op.dirname(self.prm_file),Path(self.prm_file).stem+".as")
        if op.isfile(as_file):
            logger.info(f"CmCavity has already been run, .as file present: {as_file}")
            return
        os.chdir(op.dirname(self.prm_file))
        cav_cmd = f"{self.dock_param.CmCavity_exe} -r {op.basename(self.prm_file)} -W -d"


        #pro = subprocess.Popen(cav_cmd, text=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        #for line in pro.stdout:
        #    logger.info(line.rstrip())
        pro = subprocess.run(cav_cmd, capture_output=True,text=True, shell=True)
        logger.info("Running CmCavity:")
        logger.info(cav_cmd)
        logger.info(pro.stdout)
        logger.info(pro.stderr)
        #print("return code: ",pro.returncode)

        os.chdir(RUNNING_DIRECTORY)
        return pro.returncode

    def Prepare_Make_Cavity(self):
        global logger
        self.print()
        
        if not op.isdir(self.subdir):
            os.makedirs(self.subdir,exist_ok=True)
            
        if not op.isfile(self.prm_file):
            if not op.isfile(self.receptor_file):
                if not op.isfile(self.PDB_file):
                    self.Download_PDB_file()           
                self.Prepare_Receptor_File()
            self.Make_Prm_File()
        self.Run_CmCavity()



    def Dock_With_Ligand(self,ligand_file):
        global logger
        as_file = op.join(op.dirname(self.prm_file),Path(self.prm_file).stem+".as")

        if not op.isfile(self.prm_file):
            logger.warning(f"Error, CmDock no prm file {self.prm_file}")
            return
        if not op.isfile(as_file):
            logger.warning(f"Error, CmCavity has not been run, no .as file {as_file}")
            return
        
        
        os.chdir(op.dirname(self.prm_file))

        if self.dock_param.n_best_poses > 0:
            b_str = f" -b {self.dock_param.n_best_poses} "
        else:
            b_str = " "
                
        prm_file_ne = Path(self.prm_file).stem
        lig_file_ne = Path(ligand_file).stem
        out_name = f"Dock_{prm_file_ne}_{lig_file_ne}.sdf"
        out_file = op.join(self.subdir,out_name)
        dock_cmd = f"{self.dock_param.CmDock_exe} -r {op.basename(self.prm_file)} -p dock.prm -i {ligand_file} -n {self.dock_param.n_docking_runs}{b_str}-o {out_file}"
        pro = subprocess.run(dock_cmd,
            capture_output=True,text=True, shell=True)
        logger.info(dock_cmd)
        logger.info(pro.stdout)
        logger.info(pro.stderr)
        self.Parse_Result_sdf_File(out_file,self.dock_param.dock_score_parameter,self.dock_param.main_results_file)
        

        #print("return code: ",pro.returncode)
        return pro.returncode

    def Parse_Result_sdf_File(self,sdf_file,parameter,docking_res_out_file):
        global logger
        with open(sdf_file,"r") as sdf:
            lines = [ i.strip() for i  in sdf.readlines()]
        os.chdir(RUNNING_DIRECTORY)
        
        c = 0
        #end_m = False
        name = ""
        #end_m_str = "M  END"
        entry_str = ">  <"
        end_str = "$$$$"
        entry_dict = {}

        ligand_name_entries = {}
        lig_nr = 0
        for ln_i,line in enumerate(lines):
            if c == 0:
                name = line
            c += 1
            
            if line == end_str:
                lig_nr += 1
                if name in ligand_name_entries:
                    #ligand_name_entries[name]  += [[ligand_name_entries[name][-1][0]+1 ,entry_dict]]
                    ligand_name_entries[name]  += [[lig_nr ,entry_dict]]
                
                else:
                    #ligand_name_entries[name]  = [[1,entry_dict]]
                    ligand_name_entries[name]  = [[lig_nr,entry_dict]]
                name = ""
                c = 0
                entry_dict = {}
                continue
            
            if entry_str in line:
                entry = line.replace(entry_str,"")
                entry = entry.replace(">","").lower()
                
                entry_dict[entry] = lines[ln_i+1]
        

        for lig_name,ligand_entries in ligand_name_entries.items():
            min_i = None
            min_min_score = 1000000
            min_score = min_min_score
            for i,entry_dict in ligand_entries:
                try:
                    score = eval(entry_dict.get(parameter.lower()))
                except:
                    score = min_min_score
                if score < min_score:
                    min_score = score
                    min_i = i

            out_str = f"{self.receptor_name} {lig_name} {min_score} {parameter} {sdf_file} {min_i}\n"
            #logger.info(f"cwd, {os.getcwd()}, {docking_res_out_file}")

            with open(docking_res_out_file,"a") as outf:
                outf.write(out_str)
            
            if min_score == min_min_score:
                logger.warning(f"Did not find approprite docking scores ({parameter}) for ligand name {lig_name} with receptor {self.receptor_name} in file {sdf_file}")
            else:
                logger.info(f"Found minimum docking score, receptor {self.receptor_name}, ligand {lig_name}, min score {min_score}, score parameter {parameter}, file {sdf_file}, ligand number in file {min_i}")

def Read_Receptor_Input(receptor_ini_file):
    global logger, DOCKING_DIRECTORY

    if op.isfile(receptor_ini_file):
        receptors = configparser.ConfigParser()
        receptors.read(receptor_ini_file)
    else:
        logger.warning(f"Receptor file {receptor_ini_file} does not exist")
        return []
    
    default_radius = 10.0
    default_small_sphere = 1.5
    default_max_cavities = 1
    default_vol_increase = 0.0
    default_grid_step = 0.5
    default_min_volume = 100.0
    default_n_docking_runs = 20
    default_large_sphere = 10
    default_CmCavity_exe = "cmcavity"
    default_CmDock_exe = "cmdock"
    default_dock_score_parameter = "score.inter"
    default_main_results_file = "Docking_Results.dat"




    receptor_list = []
    for rec in receptors.sections():
        # Cavity values
        radius = receptors[rec].getfloat("radius",default_radius)
        small_sphere = receptors[rec].getfloat("small_sphere",default_small_sphere)
        max_cavities = receptors[rec].getint("max_cavities",default_max_cavities)
        vol_increase = receptors[rec].getfloat("vol_increase",default_vol_increase)
        grid_step = receptors[rec].getfloat("grid_step",default_grid_step)
        min_volume = receptors[rec].getfloat("min_volume",default_min_volume)
        large_sphere = receptors[rec].get("large_sphere",default_large_sphere)
        two_sphere_center = receptors[rec].get("two_sphere_center")
        reference_ligand = receptors[rec].get("reference_ligand")
        cav_param = Cavity_Parameters(radius,small_sphere,max_cavities,vol_increase,grid_step,min_volume,two_sphere_center,reference_ligand,large_sphere)


        # Docking values
        n_docking_runs = receptors[rec].getint("n_docking_runs",default_n_docking_runs)
        n_best_poses = receptors[rec].getint("n_best_poses",-1)
        CmCavity_exe = receptors[rec].get("CmCavity_exe",default_CmCavity_exe)
        CmDock_exe = receptors[rec].get("CmDock_exe",default_CmDock_exe)
        ligand_dat = receptors[rec].get("ligand_dat","")
        dock_score_parameter = receptors[rec].get("dock_score_parameter",default_dock_score_parameter)
        main_results_file = receptors[rec].get("main_results_file",op.join(RUNNING_DIRECTORY,default_main_results_file))

        dock_param = Docking_Parameters(n_docking_runs,n_best_poses,CmCavity_exe,CmDock_exe,ligand_dat,dock_score_parameter,main_results_file)
        # Receptor values
        chains_kept = receptors[rec].get("chains_kept","").split()
        while "" in chains_kept:
            chains_kept.remove("")
        ligands_kept = receptors[rec].get("ligands_kept","").split()
        while "" in ligands_kept:
            ligands_kept.remove("")

        PDB_file = receptors[rec].get("PDB_file","")
        PDB_ID = receptors[rec].get("PDB_ID","")
        receptor_file = receptors[rec].get("receptor_file","")
        prm_file = receptors[rec].get("prm_file","")
        subdir = receptors[rec].get("subdir","")
        if not subdir: # subdir = Docking_dir/name
            subdir = op.join(op.abspath(DOCKING_DIRECTORY),rec)
        elif not op.isdir(subdir): # subdir = Docking_dir/ subdir
            subdir = op.join(op.abspath(DOCKING_DIRECTORY),subdir)           
        else: # subdir = subdir
            pass


        receptor = Receptor(rec,PDB_ID,chains_kept,ligands_kept,cav_param,dock_param,PDB_file,receptor_file,prm_file,subdir)
        receptor_list.append(receptor)
    
    return receptor_list





def Read_Ligand_Input(ligand_dat):
    #os.chdir(DOCKING_DIRECTORY)
    global logger
    if op.isfile(ligand_dat):
        with open(ligand_dat,"r") as lig_dat:
            ligand_list = [ln.strip() for ln in lig_dat.readlines() if (ln.strip() and ln.strip()[0] != "#")]
    else:
        logger.warning(f"Ligand file {ligand_dat} does not exist")
        return []
    
    to_rm = []
    for lig in ligand_list:
        if not op.isfile(lig):
            logger.warning(f"{lig} in lignand list, is not a file, skipping")
            to_rm.append(lig)
    for lig in to_rm:
        if lig in ligand_list:
            ligand_list.remove(lig)


    return ligand_list




def Run_Receptor_Cavity(receptor):
    global logger
    receptor.Prepare_Make_Cavity()
    global Receptor_Ligand_List,logger
    Get_Receptor_Ligand_List(receptor)

  
def Get_Receptor_Ligand_List(receptor):
    global logger
    global Receptor_Ligand_List
    logger.info(f"Reading ligand file for receptor")
    ligand_list = Read_Ligand_Input(receptor.dock_param.ligand_dat)


    for lig in ligand_list:
        Receptor_Ligand_List.append([receptor,lig])

    logger.info(f"Read ligand file, found {len(ligand_list)} ligands_kept")
    


def Run_Receptor_Lignad_Dock(rec_lig):
    global logger
    receptor,ligand = rec_lig
    receptor.Dock_With_Ligand(ligand)





def init_globals(args,rll):
    global DOCKING_DIRECTORY,PDBS_DIRECTORY,Receptor_Ligand_List,logger
    if args.run_dir:
        DOCKING_DIRECTORY = args.run_dir
    Receptor_Ligand_List = rll
    process = multiprocessing.current_process()

    if args.pdb_dir:
        PDBS_DIRECTORY = args.pdb_dir
    PDBS_DIRECTORY = op.abspath(PDBS_DIRECTORY)
    DOCKING_DIRECTORY = op.abspath(DOCKING_DIRECTORY)
    # create a logger
    logger = logging.getLogger(str(process.name))
    log_file = args.log_file
    if log_file:
        logging.basicConfig(filename=log_file)
    else:
        logging.basicConfig()

    # log all messages, debug and up
    logger.setLevel(logging.DEBUG)
    # get the current process
    # report initial message

    logger.info(f'{process.name} starting.')



if __name__ == "__main__":


    # Create the parser
    my_parser = argparse.ArgumentParser(description='CmDock wrapper')

    # Add the arguments
    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('--ini_file',"-i", action='store', type=str, required=True,help='The input file file, formated as an .ini file')
    my_parser.add_argument('--run_dir',"-d", action='store', type=str, help='The directory with docking runs and file, default ./Docking')
    my_parser.add_argument('--pdb_dir',"-pd", action='store', type=str, help='The directory where pdb files are downloaded, default ./PDBs')
    my_parser.add_argument('--n_proc',"-n", action='store', type=int, help='Number of processes started, default 1')
    my_parser.add_argument('--log_file',"-l", action='store', type=str, help='Log file')

    args = my_parser.parse_args()

    start_time = time.time()

    
    log_file = args.log_file
    if log_file:
        open(log_file,"w").close()
    manager = multiprocessing.Manager()
    Receptor_Ligand_List = manager.list()
    init_globals(args,Receptor_Ligand_List)
    receptor_list = Read_Receptor_Input(args.ini_file)

    #logger = logging.getLogger()
    #logger.setLevel(logging.DEBUG)
    #print("run",logger)
    
    # report initial message

    os.makedirs(DOCKING_DIRECTORY, exist_ok=True)
    os.makedirs(PDBS_DIRECTORY, exist_ok=True)

    if not args.n_proc:
        args.n_proc = 1
    if multiprocessing.cpu_count() < args.n_proc:
        args.n_proc = multiprocessing.cpu_count()

    if no_pymol:
        logger.info("PyMOL could not be imported, receptor/reference ligand file preperation will not function")

    logger.info("\nStarting Receptor Preperation\n")
    with multiprocessing.Pool(processes=args.n_proc,initializer=init_globals,initargs=(args,Receptor_Ligand_List,)) as pool:
        #pool.map(tst,receptor_list)
        pool.map(Run_Receptor_Cavity,receptor_list)
    logger.info("\nReceptor Preperation Done!\n")

    #for rec in receptor_list:
    #    Get_Receptor_Ligand_List(rec)

    logger.info("\nStarting Receptor Ligand Docking\n")

    if len(Receptor_Ligand_List) > 0:
        with multiprocessing.Pool(processes=args.n_proc,initializer=init_globals,initargs=(args,Receptor_Ligand_List,)) as pool:
            pool.map(Run_Receptor_Lignad_Dock,Receptor_Ligand_List)
        logger.info("\nReceptor Docking Done!\n")

    process = multiprocessing.current_process()

    logger.info(f'{process.name} done in {time.time() - start_time}s')
