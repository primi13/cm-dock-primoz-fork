#!/usr/bin/python3
# -*- coding: utf-8 -*-
# CmDock - cmcalcgrid
# v mj0.2
#
# go

import os
import subprocess
import sys
from pathlib import Path

# default settings for cmcalcgrid
SET_GRIDSTEP = 0.3
SET_BORDER = 1.0

# if you don't have the env set, modify the next line
CMDOCK_ROOT = Path(os.getenv("CMDOCK_ROOT", os.getcwd())).resolve()


def cmcalcgrid(files):
    print("CMDOCK_ROOT:", CMDOCK_ROOT, file=sys.stderr)
    for i, file in enumerate(files, 1):
        path = Path(file).resolve()
        for j, num in enumerate([1, 5]):
            cmdline = [CMDOCK_ROOT / "build" / "cmcalcgrid", f"-r{path}",
                       f"-pcalcgrid_vdw{num}.prm", f"-o_wdw{num}.grd",
                       f"-g{SET_GRIDSTEP}", f"-b{SET_BORDER}"]
            print(f"({i+j}/{len(files) * 2})", *cmdline, file=sys.stderr)
            subprocess.run(cmdline, check=1)


# check argvs
if len(sys.argv) <= 1:
    print("-" * 40, file=sys.stderr)
    print("Please add grid files as script arguments in command line.",
          file=sys.stderr)
    print("cmcalcgrid will calculate grids for all receptors defined in script "
          "arguments", file=sys.stderr)
    print("-" * 40, file=sys.stderr)
    sys.exit(2)

# go cmcalcgrid
cmcalcgrid(sys.argv[1:])
