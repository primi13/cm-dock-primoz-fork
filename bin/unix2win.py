#!/usr/bin/python3
# -*- coding: utf-8 -*-
# CmDock
# v mj0.1
# 
# go

import sys, getopt

#linux ali osx v win
new_line_char = "\r\n"

def main(argv):

    if len(sys.argv) == 1:
        print("Use -h switch")
    else:
        pass

    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print ('win2unix.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("-" * 50)
            print("""script command line arguments should be the \n
            files you want to convert line endings from unix/linux/osx to win""")
            print ('win2unix.py -i <inputfile> -o <outputfile>')
            print("-" * 50)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print ("Input file is: ", inputfile)
    print ("Output file is: ", outputfile)

    with open(inputfile, "r") as f:
        f_text = f.read()
        f_text = new_line_char.join(f_text.splitlines())
    with open(outputfile, "w") as fo:
        fo.write(f_text + "\r\n")

if __name__ == "__main__":
 main(sys.argv[1:])


"""
SCHOOL:

CR and LF

The American Standard Code for Information Interchange (ASCII)
defined control-characters including CARRIAGE-RETURN (CR) and
LINE-FEED (LF) that were (and still are) used to control the
print-position on printers in a way analogous to the mechanical
typewriters that preceded early computer printers.

PLATFORM DEPENDENCY

* In Windows the traditional line-separator in text files is CR
followed by LF.

* In old (pre OSX) Apple Macintosh systems the traditional line
separator in text files was CR.

* In Unix and Linux, the traditional line-separator in text
files is LF.

\n AND \r

In many programming and scripting languages \n means "new line".
Sometimes (but not always) this means the ASCII LINE-FEED
character (LF), which, as you say, moves the cursor (or print
position) down one line. In a printer or typewriter, this would
actually move the paper up one line.

Invariably \r means the ASCII CARRIAGE-RETURN character (CR)
whose name actually comes from mechanical typewriters where
there was a carriage-return key that caused the roller
("carriage") that carried the paper to move to the right,
powered by a spring, as far as it would go. Thus setting the
current typing position to the left margin.

PROGRAMMING

In some programming languages \n can mean a platform-dependent
sequence of characters that end or separate lines in a text file.
For example in Perl, print "\n" produces a different sequence of
characters on Linux than on Windows. In Java, best practise, if
you want to use the native line endings for the runtime platform,
is not to use \n or \r at all. You should use System.getProperty
("line.separator"). You should use \n and \r where you want LF
and CR regardless of platform (e.g. as used in HTTP, FTP and
other Internet communications protocols).

UNIX stty

In a Unix shell, the stty command can be used to cause the shell
to translate between these various conventions. For example:
stty -onlcr will cause the shell to subsequently translate all
outgoing LFs to CR LF.

Linux and OSX follow Unix conventions
"""
